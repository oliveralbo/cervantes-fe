import { Routes, Route, Navigate } from 'react-router-dom'
import Layout from './components/Layout/Layout'
import Home from './pages/Home'
import Activities from './pages/Activities'
import History from './pages/History/History'

export default function App() {
    return (
        <Layout>
            <Routes>
                <Route path="/" element={<Navigate to={'/inicio'} />} />
                <Route path="inicio" element={<Home />} />
                <Route path="actividades" element={<Activities />} />
                <Route path="historia" element={<History />} />
            </Routes>
        </Layout>
    )
}
