import React from 'react'
import PropTypes from 'prop-types'
import NavBar from '../NabBar/NavBar'

function Layout({ children }) {
    return (
        <>
            <NavBar />
            <div style={{ margin: '16px' }}>{children}</div>
        </>
    )
}

Layout.propTypes = {
    children: PropTypes.any,
}

export default Layout
