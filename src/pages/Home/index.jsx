import * as React from 'react'
import { styled } from '@mui/material/styles'
import { Fade } from 'react-awesome-reveal'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import SectionOne from './SectionOne'
import SectionTwo from './SectionTwo'

function Home() {
    return (
        <Grid container spacing={2}>
            {/* <Fade cascade style={{ width: '100%' }}> */}
            <Grid item xs={12}>
                {/* <Fade direction="right"> */}
                <SectionOne />
                {/* </Fade> */}
            </Grid>
            <Grid item xs={12}>
                {/* <Fade direction="left"> */}
                <SectionTwo />
                {/* </Fade> */}
            </Grid>
            <Grid item xs={12}>
                {/* <Fade direction="right"> */}
                <SectionTwo />
                {/* </Fade> */}
            </Grid>
            <Grid item xs={12}>
                {/* <Fade direction="left"> */}
                <SectionTwo />
                {/* </Fade> */}
            </Grid>
            {/* </Fade> */}
        </Grid>
    )
}

export default Home
