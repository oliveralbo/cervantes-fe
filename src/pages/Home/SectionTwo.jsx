import * as React from 'react'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'

export default function SectionTwo() {
    return (
        <Box height={'400px'}>
            <Paper
                style={{
                    backgroundPosition: 'left',
                    width: '116%',
                    backgroundSize: '88%',
                    height: '100%',
                    backgroundImage: `url(${'https://thumbs.dreamstime.com/b/textura-abstracta-de-una-nueva-pared-limpia-los-bloques-blancos-del-ladrillo-113568943.jpg'})`,
                }}
            >
                hola
            </Paper>
        </Box>
    )
}

// https://listadetareas.s3.sa-east-1.amazonaws.com/cancha.jpg
