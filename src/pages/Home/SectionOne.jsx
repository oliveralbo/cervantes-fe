import * as React from 'react'
import Box from '@mui/material/Box'
import Paper from '@mui/material/Paper'
import Grid from '@mui/material/Grid'
import logo from '../../assets/img/cervantes.png'
import { Slide } from 'react-awesome-reveal'
import Typography from '@mui/material/Typography'

const styles = {
    paper: {
        backgroundPosition: 'left',
        width: '116%',
        backgroundSize: '88%',
        height: '100%',
        backgroundImage: `url(https://listadetareas.s3.sa-east-1.amazonaws.com/cancha.jpg)`,
    },
    gridLogo: {
        alignItems: 'center',
        display: 'flex',
        justifyContent: 'center',
    },
}

export default function SectionOne() {
    return (
        <Box height={'400px'}>
            <Paper sx={styles.paper}>
                <Grid container spacing={2} sx={{ height: '100%' }}>
                    <Grid item xs={3} sx={styles.gridLogo}>
                        <Slide direction="right">
                            <img src={logo} />
                        </Slide>
                    </Grid>
                    <Grid item xs={9} sx={{ marginTop: 8 }}>
                        <Typography variant="h1" gutterBottom>
                            Club Cervantes
                        </Typography>
                        <Typography variant="h2" gutterBottom>
                            La papelera
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        </Box>
    )
}
